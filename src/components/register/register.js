import React, { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'

import useForm from './useForm.js';

const Register = props => {
    const submitRegister = (e) => {

        const { username, email, password } = values
        axios.post("http://localhost:8666/register", {
             username,
             email,
             password  
            })
            .then(res => {
                notifySuccess(res.data.msg)
                props.history.push('/login')
            })
            .catch(err => {
                console.log(err)
                toast('This user already exist.')
                notifyFailure()
            })
    }

    const [ passwordVisibility, setPasswordVisibility ] = useState(false)

    const togglePasswordVisibility = () => {
      setPasswordVisibility(!passwordVisibility)
    }

    const routeChange = () => {
      props.history.push("/login");
    }
    const { values, handleChange, handleSubmit } = useForm(submitRegister)

    const notifySuccess = (msg) => { toast.success(msg) }
    const notifyFailure = (msg) => { toast.error(msg) }

    return (
    <div className="container-login container right-panel-active">
  
      <div className="form-container sign-up-container login-form">
        <form onSubmit={handleSubmit}>
          <h1>Sign Up</h1>
        <div className="group">      
          <input type="text" name="username" id="username" value={values.username} onChange={handleChange} required/>
          <span className="highlight"></span>
          <span className="bar"></span>
          <label>Username</label>
        </div>
        <div className="group">      
          <input type="text" name="email" id="email" value={values.email} onChange={handleChange} required/>
          <span className="highlight"></span>
          <span className="bar"></span>
          <label>Email</label>
        </div>
        <div className="group">      
          <input type={passwordVisibility ? "text" : "password"} id="password" name="password" value={values.password} onChange={handleChange} required/>
          <span className="highlight"></span>
          <span className="bar"></span>
          <label>Password</label>
          <Link to="#" onClick={togglePasswordVisibility}>Show / Hide password</Link>
        </div>
        <div className="group-join"> 
          <button>Join us !</button>
        </div>
        </form>
      </div>
      
      <div className="overlay-container">
        <div className="overlay">
          <div className="overlay-panel overlay-left">
            <h1>Already a member ?</h1>
            <p>Login to access your profile</p>
            <button className="ghost" id="signIn" onClick={routeChange} >Log in here</button>
          </div>
        </div>
      </div>
      
    </div>
    );
}

export default Register;