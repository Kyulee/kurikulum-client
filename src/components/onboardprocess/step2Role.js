import React, { Component } from 'react';
import { withRouter } from 'react-router';
import '../stylesheets/onboardprocess/onboardprocess.scss';
import { Card, CardText, CardTitle } from 'reactstrap';

class Role extends Component {

  render() {
    const { history } = this.props;
    return (
      <div className="role onboard">
        <img className="book" src="https://image.flaticon.com/icons/png/512/1081/1081007.png" alt=" book step 2 " />
        <h2>Sélectionner votre rôle</h2>
        <p>Tout d'abord, dites-nous quel travail avez vous, ou voulez vous effectué, afin que nous puissons
                    <br />
          vous aider à trouver des compétences ou des sujets que vous pourriez vouloir apprendre.
                </p>
        <div className="searchbar-inner">
          <div className="searchbar-input-wrap">
            <input type="search" placeholder="Search here..." />
            <i className="searchbar-icon"></i>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <Card body outline color="info" className="col">
              <CardTitle>This is a role</CardTitle>
              <CardText>Text who explain what's the role do.</CardText>
            </Card>
            <Card body outline color="info" className="col">
              <CardTitle>This is a role</CardTitle>
              <CardText>Text who explain what's the role do.</CardText>
            </Card>
            <Card body outline color="info" className="col">
              <CardTitle>This is a role</CardTitle>
              <CardText>Text who explain what's the role do.</CardText>
            </Card>
          </div>
          <div className="row">
            <Card body outline color="info" className="col-md-4">
              <CardTitle>This is a role</CardTitle>
              <CardText>Text who explain what's the role do.</CardText>
            </Card>
            <Card body outline color="info" className="col-md-4">
              <CardTitle>This is a role</CardTitle>
              <CardText>Text who explain what's the role do.</CardText>
            </Card>
            <Card body outline color="info" className="col-md-4">
              <CardTitle>This is a role</CardTitle>
              <CardText>Text who explain what's the role do.</CardText>
            </Card>
          </div>
          <div className="row">
            <Card body outline color="info" className="col-md-4">
              <CardTitle>This is a role</CardTitle>
              <CardText>Text who explain what's the role do.</CardText>
            </Card>
            <Card body outline color="info" className="col-md-4">
              <CardTitle>This is a role</CardTitle>
              <CardText>Text who explain what's the role do.</CardText>
            </Card>
            <Card body outline color="info" className="col-md-4">
              <CardTitle>This is a role</CardTitle>
              <CardText>Text who explain what's the role do.</CardText>
            </Card>
          </div>
        </div>
        <button className="skipbutton" onClick={() => { history.push("/welcome/step3") }}>
          Skip this step <img className="skip" src="https://image.flaticon.com/icons/svg/248/248400.svg" alt="skip" />
                    </button>
        <div className="row">
          <button className="col backbutton" onClick={() => { history.push("/welcome/step1") }}>
            &#x2190; Go back
                    </button>

          <button className="col nextbutton" onClick={() => { history.push("/welcome/step3") }}>
            Next step &#x2192;
                    </button>
        </div>
      </div>
    )
  }
}

export default withRouter(Role);
