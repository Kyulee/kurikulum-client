import React, { Component } from 'react';
import { withRouter } from 'react-router';


class Ending extends Component {
    render() {
        const { history } = this.props;

        return (
            <div className="ending onboard">
            <img className="end" alt="endingstep" src="https://image.flaticon.com/icons/png/512/1081/1081045.png"></img>
            <h2>Vous avez terminé !</h2>
            <h3>Thanks you !</h3>
            <div className="row">
            <button className="col backstep5" onClick={() => {history.push("/welcome/step4")}}>
            &#x2190; Go back
        </button>

        <button className="col endingbutton" onClick={() => {history.push("/user/dashboard")}}>
        <i className="fas fa-check"></i>Im ready to learn !
        </button>
        </div>
        </div>
        )
    }
}

export default withRouter(Ending);