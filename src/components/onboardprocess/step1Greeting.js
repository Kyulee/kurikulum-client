import React, { Component } from 'react';
import { withRouter } from 'react-router';
import '../stylesheets/onboardprocess/onboardprocess.scss';
import Image from "../images/greetingbus.gif";


class Greeting extends Component {
    render() {
        const { history } = this.props;
        return (
            <div className="greeting onboard">
                <h1>
                    Bienvenue sur Kurikulum
                    </h1>
                <img className="bus" src={Image} alt="busstep1" />
                <div>
                    <h2>C'est ici, que tout commence !</h2>
                </div>
                <div className="learn">
                    <h3 >Une base de données, toujours enrichie</h3>
                    <p >Nous vous laissons toutes les ressources nécessaires <br />
                        pour apprendre et progresser, comme des millions de cours,<br />
                        de vidéos, de livres et tant d'autres !</p>
                        
                </div>
                <button className="startbutton" onClick={() => { history.push("/welcome/step2") }}>
                    Let's go ! &#x2192;
            </button>
            </div>
        )
    }
}

export default withRouter(Greeting);
