import React, { Component } from 'react';
import { withRouter } from 'react-router';

class Subject extends Component {
   constructor(props){
       super(props);
       this.state = {isSelected: true};

       this.handleClick = this.handleClick.bind(this)
   }

   handleClick() {
       this.setState(state => ({
           isSelected: !state.isSelected
       }));
   }
    render() {
        const { history } = this.props;
        return (
            <div className="subject onboard">
                <img className="skill" src="https://image.flaticon.com/icons/png/512/1081/1081010.png" alt="skillstep3" />
                <h1>Sélectionnez les compétences que vous souhaitez développer</h1>
                <p>Maintenant, choisissez quelques compétences que vous aimeriez <br />
                    développer afin que nous puissions mieux organiser votre expérience d'apprentissage.</p>

                <h3>Compétences succeptible de vous interessés</h3>
                <button  type="button" className="btn btn-primary" onClick={() => this.handleClick()}>
                    {this.state.isSelected ? '' : <i className="fas fa-check"></i> } Python
                </button>
                <button  type="button" className="btn btn-primary" onClick={() => this.handleClick()}>
                    {this.state.isSelected ? '' : <i className="fas fa-check"></i> } React
                </button>
                <div className="searchbar-inner">
                    <h3>Vous recherchez quelque chose de spécifique ?</h3>
                    <div className="searchbar-input-wrap">
                        <input type="search" placeholder="Search here..." />
                        <i className="searchbar-icon"></i>
                    </div>
                </div>
                <button className="skipbutton" onClick={() => { history.push("/welcome/step4") }}>
                        Skip this step <img className="skip" src="https://image.flaticon.com/icons/svg/248/248400.svg" alt="skip" />
                    </button>
                <div className="row">
                    <button className="col backbutton" onClick={() => { history.push("/welcome/step2") }}>
                        &#x2190; Go back
                    </button>

                    <button className="col nextbutton" onClick={() => { history.push("/welcome/step4") }}>
                        Next step &#x2192;
                    </button>
                </div>
            </div>
        )
    }
}

export default withRouter(Subject);