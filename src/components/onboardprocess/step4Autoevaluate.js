import React, { Component } from 'react';
import { withRouter } from 'react-router';
import  Rating  from 'react-rating';


class Autoevaluate extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            skill: 'REACT'
        }
    }
    render() {
        const { history } = this.props;
        return (
            <div className="autoevaluate onboard">
                <div>
            <img className="rate" alt="ratestep4" src="https://image.flaticon.com/icons/png/512/1080/1080969.png"></img>
                <h4>Tu as selectionné la compétences {this.state.skill}</h4>
                <div>
                <p>How do you feel about {this.state.skill} ?</p>
                <Rating
                start={11}
                stop={1}
                step={-2}
                />
                </div>
                <div>
                    <button>Submit</button>
                </div>
                </div>
                <button className="skipbutton" onClick={() => { history.push("/welcome/step5") }}>
                        Skip this step <img className="skip" src="https://image.flaticon.com/icons/svg/248/248400.svg" alt="skip"/>
                    </button>
                <div className="row">
                    <button className="col backbutton" onClick={() => { history.push("/welcome/step3") }}>
                        &#x2190; Go back
                    </button>

                    <button className="col nextbutton" onClick={() => { history.push("/welcome/step5") }}>
                        Next step &#x2192;
                    </button>
                </div>
            </div>
        )
    }
}

export default withRouter(Autoevaluate);