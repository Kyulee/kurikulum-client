import React, { Component } from 'react';
import '../stylesheets/onboardprocess/onboardprocess.scss';
import Greeting from './step1Greeting.js';
import Role from './step2Role.js';
import Subject from './step3Subject.js';
import Autoevaluate from './step4Autoevaluate.js';
import Ending from './step5Ending.js';

import { Switch, Route, Redirect, withRouter } from 'react-router-dom';


class Onboardprocess extends Component {
    componentDidMount() {
        const { history } = this.props;

        window.addEventListener("popstate", () => {
            history.go(1);
        });
    }

    render() {
        return (
            <Switch>
                <Route exact path="/welcome" render={() => <Redirect to="/welcome/step1" />} />
                <Route path="/welcome/step1" component={Greeting} />
                <Route path="/welcome/step2" component={Role} />
                <Route path="/welcome/step3" component={Subject} />
                <Route path="/welcome/step4" component={Autoevaluate} />
                <Route path="/welcome/step5" component={Ending} />
            </Switch>
        );
    }
}

export default withRouter(Onboardprocess);