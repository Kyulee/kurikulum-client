import React, { Component } from 'react';
import { Tab, Tabs } from 'react-bootstrap';

import '../stylesheets/course/uploadcourse.scss';

class UploadCourse extends Component {
    render () {
        return (
        <div>
            <img id="uploadcours" className="center" alt="uploadcours" src="https://image.flaticon.com/icons/png/512/262/262530.png" />
            <h1>Upload a course</h1>
            <div className="uploadtab">
            <Tabs defaultActiveKey="article" id="tabs">
                 <Tab eventKey="article" title="Article">
                     <br />
                     <i className="far fa-newspaper fa-3x" />
                    <h1>Article</h1>
                    <input id='upinputarticle' placeholder="Put your link here" />
                </Tab>
                <Tab eventKey="video" title="Video" >
                    <br />
                    <i className="fas fa-video fa-3x" />
                    <h1>Video</h1>
                    <input id='upinputvideo' placeholder="Put your link here" />
                </Tab>
                <Tab eventKey="livre" title="Livre" >
                    <br />
                    <i className="fas fa-book fa-3x" />
                    <h1>Book</h1>
                    <input id='upinputlivre' placeholder="Put your title here" />
                </Tab>
                <Tab eventKey="podcast" title="Podcast" >
                    <br />
                    <i className="fas fa-podcast fa-3x" />
                    <h1>Podcast</h1>
                    <input id='upinputpodcast' placeholder="Put your link here" />
                </Tab>
            </Tabs>
            </div>
        </div>
        )
    }
}

export default UploadCourse;