import React, { Component } from 'react';
import '../stylesheets/course/searchcourse.scss';
// import Image from '../images/coding.png';
// import Image1 from '../images/marketing.png';
// import Image2 from '../images/ecommerce.png';
import Filter from './filter.js';
import SuggestCourse from './suggestCourse';
import Rating from 'react-rating';

class SearchCourse extends Component {
	constructor(props) {
		super(props);
		this.state = {
			toggle: true,
			courses: [
				{
					id: 1,
					image: require('../images/coding.png'),
					type: 'Article',
					duration: '20',
					title: 'Bases de C',
					description: 'Voici les bases du C.',
					skill: 'C, Programming',
					difficulty: 'Beginner',
					rate: '4'
				},
				{
					id: 2,
					image: require('../images/marketing.png'),
					type: 'Video',
					duration: '60',
					title: 'Marketing en 1 heure',
					description: 'Marketing, Vendre.',
					skill: 'Marketing, Selling',
					difficulty: 'Intermediate',
					rate: '4.5'
				},
				{
					id: 3,
					image: require('../images/ecommerce.png'),
					type: 'Podcast',
					duration: '10',
					title: 'Référencement web',
					description: 'Know all about Google Trends ',
					skill: 'SEO, SMO ',
					difficulty: 'Advanced',
					rate: '3.75'
				},
				{
					id: 4,
					image: require('../images/coding.png'),
					type: 'Article',
					duration: '15',
					title: 'React',
					description: 'How to display your array ?',
					skill: 'React, Programming',
					difficulty: 'Expert',
					rate: '5'
				},
				{
					id: 5,
					image: require('../images/coding.png'),
					type: 'Podcast',
					duration: '130',
					title: 'Python',
					description: 'Bases du python',
					skill: 'Python, Programming',
					difficulty: 'Beginner',
					rate: '4.5'
				},
				{
					id: 6,
					image: require('../images/coding.png'),
					type: 'Article',
					duration: '60',
					title: 'React Native',
					description: 'Créer une application en 1h',
					skill: 'React Native, Programming',
					difficulty: 'Intermediate',
					rate: '4'
				}
			]
		};
		this.handleToggleClick = this.handleToggleClick.bind(this);
	}

	handleToggleClick() {
		this.setState((state) => ({
			toggle: !state.toggle
		}));
	}
	render() {
		return (
			<div className="main">
				<div className="searchingbar">
					<Filter />
					<SuggestCourse />
				</div>
				<ul className="cards">
					{this.state.courses.map((data) => (
						<li className="cards_item" key={data.id}>
							<div className="card">
								<div className="difficulty" id={data.difficulty} >
									{data.difficulty}
								</div>
								<div className="card_image">
									<img className="cardimg" alt="dataimg" src={data.image} />
								</div>
								<div className="card_content">
									<h3 className="headcard">{data.type} - {data.duration} minutes</h3>
									<h2 className="card_title">{data.title}</h2>
									<p className="card_text">{data.description}</p>
									<p className="iconskill">
										<img className="improveskill" alt="improveskill" src="https://image.flaticon.com/icons/svg/1420/1420033.svg"/>
										<br />{data.skill}</p>
										<div className="starRate">
										<Rating className="starRate"
											initialRating={data.rate}
											readonly={true}
  											emptySymbol={<i className="far fa-star" />}
											fullSymbol={<i className="fas fa-star" />}
											/><p className="star">{data.rate}/5</p>
										</div>
									<button className="btn card_btn">Read More</button>
									<input placeholder="  Commentez cette article" />
								</div>
							</div>
						</li>
					))}
				</ul>
			</div>
		);
	}
}

export default SearchCourse;
