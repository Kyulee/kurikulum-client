import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../stylesheets/course/allcourse.scss';
import Select from 'react-select';

const categorie = [
    { value: 'programming', label: 'Programming' },
    { value: 'marketing', label: 'Marketing' },
    { value: 'ecommerce', label: 'E-commerce' },
    { value: 'organization', label: 'Organization' }
];

class Allcourses extends Component {

    render() {
        return (
            <div className="allcourses">
                <h2>All courses</h2>
                <div className="input-icons">
                    <div className="wrapper">
                    <Select className="select searchbarcourse"
                        closeMenuOnSelect={false}
                        isMulti
                        options={categorie}
                    />
                        <p className="textallcourse">You can't find your course ? Tell us</p>
                    </div>
                </div>
                <div className="row cat">
                    <div className="catone col-6">
                <Link><img className="iconbase" alt="firstimg" src="https://image.flaticon.com/icons/svg/993/993717.svg" /> <h2>Programming</h2></Link>
                <Link to="/">C</Link> | <Link to="/">C++</Link> | <Link to="/">C#</Link> | <Link to="/">.NET</Link> | <Link to="/">PHP</Link><br/>
                <Link to="/">Python</Link> | <Link to="/">Ruby</Link> | <Link to="/">Symfony</Link> | <Link to="/">MongoDB</Link> | <Link to="/">NoSQL</Link>
                </div>
                <div className="cattwo col-6">
                <Link><img className="iconbase" alt="secondimg" src="https://image.flaticon.com/icons/svg/786/786695.svg" /> <h2>Marketing</h2> </Link>
                <Link to="/">Marketing Digital</Link> | <Link to="/">Commerce International</Link> | <Link to="/">Communication</Link><br/>
                <Link to="/">B to B</Link> | <Link to="/">B to C</Link> | <Link to="/">Business</Link> | <Link to="/">Gestion de projet</Link>
                </div>
                </div>
                <div className="row cat">
                    <div className="catone col-6">
                    <Link><img className="iconbase" alt="thirdimg" src="https://image.flaticon.com/icons/svg/679/679847.svg" /> <h2>E-commerce</h2></Link>
                <Link to="/">Bases</Link> | <Link to="/">SEO SEA SMO</Link> | <Link to="/">Framework</Link> | <Link to="/">Social Media</Link> | <Link to="/">Marketing Digital</Link><br/>
                <Link to="/">Python</Link> | <Link to="/">Ruby</Link> | <Link to="/">Symfony</Link> | <Link to="/">MongoDB</Link> | <Link to="/">NoSQL</Link>
                </div>
                <div className="cattwo col-6">
                <Link><img className="iconbase" alt="fourthimg" src="https://image.flaticon.com/icons/svg/1077/1077650.svg" /> <h2>Web Design</h2></Link>
                <Link to="/">Theme</Link> | <Link to="/">C++</Link> | <Link to="/">C#</Link> | <Link to="/">.NET</Link> | <Link to="/">PHP</Link><br/>
                <Link to="/">Python</Link> | <Link to="/">Ruby</Link> | <Link to="/">Symfony</Link> | <Link to="/">MongoDB</Link> | <Link to="/">NoSQL</Link>
                </div>
                </div>
            </div>
        )
    }
}

export default Allcourses;