import React, { Component } from 'react';
import { Button } from 'reactstrap';
import '../stylesheets/course/filter.scss';
import Select from 'react-select';


const type = [
	{ value: 'article', label: 'Article' },
	{ value: 'video', label: 'Video' },
	{ value: 'podcast', label: 'Podcast' },
	{ value: 'book', label: 'Book' }
];
const subject = [
	{ value: 'programming', label: 'Programming' },
	{ value: 'marketing', label: 'Marketing' },
	{ value: 'ecommerce', label: 'E-commerce' },
	{ value: 'organization', label: 'Organization' }
];

const difficulty = [
	{ value: 'beginner', label: 'Beginner' },
	{ value: 'intermediate', label: 'Intermediate' },
	{ value: 'advanced', label: 'Advanced' },
	{ value: 'expert', label: 'Expert' }
];

const formatGroupLabel = (data) => (
	<div>
		<span>{data.label}</span>
	</div>
);

class Filter extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value: '60'
		};
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(event) {
		this.setState({ value: event.target.value });
	}

	render() {
		return (
			<div className="sidefilter main">
				<div className="row">
					<img
						className="searchimg"
						id="search"
						alt="searchimg"
						src="https://image.flaticon.com/icons/png/512/1081/1081041.png"
					/>
					<h2 className="col">Search a course</h2>
				</div>
				<div className="filterby">
					<h3>Filter by :</h3>
				</div>
				<form>
					<div className="filters">
                        <div className="filter_item">
							<p>Type</p>
                            <Select 
                                className="select_item"
                                isMulti 
                                options={type} 
                                formatGroupLabel={formatGroupLabel} 
                            />
                            </div>
                            <div className="filter_item">
							<p>Subject</p>
							<Select
                                className="select_item"
								isMulti
								options={subject}
								formatGroupLabel={formatGroupLabel}
							/>
                            </div>
                            <div className="filter_item">
							<p>Difficulty</p>
							<Select
                                isMulti
                                className="select_item"
								options={difficulty}
								formatGroupLabel={formatGroupLabel}
							/>
                            </div>
							<div className="slidecontainer">
                            <label>Combien de temps as-tu devant toi ?</label>
							<div className="row">
							<button className="fivemin" variant="outline-primary">5 min</button>
							<button className="tenmin" variant="outline-primary">15 min</button>
							</div>
							<div className="row">
							<button className="thirtymin" variant="outline-primary">30 min</button>
							<button className="moremin" variant="outline-primary">More than 60 min</button>
								<br />
							</div>
						</div>
					</div>
					<button id="filtersearch" value="submit">
						Search
					</button>
				</form>
			</div>
		);
	}
}

export default Filter;
