import React, { Component } from 'react';
import Rating from 'react-rating';
import '../stylesheets/course/searchcourse.scss';

class SuggestCourse extends Component {
    constructor(props) {
        super(props);
        this.state = {
			courses: [
				{
					id: 4,
					image: require('../images/coding.png'),
					type: 'Article',
					duration: '15',
					title: 'React',
					description: 'How to display your array ?',
					skill: 'React, Programming',
					difficulty: 'Expert',
					rate: '5'
				},
				{
					id: 6,
					image: require('../images/coding.png'),
					type: 'Article',
					duration: '60',
					title: 'React Native',
					description: 'Créer une application en 1h',
					skill: 'React Native, Programming',
					difficulty: 'Intermediate',
					rate: '4'
				}
			]
		};
    }
    render () {
        return (
            <div className="suggest">
				<ul className="cards">
					{this.state.courses.map((data) => (
						<li className="cards_item" key={data.id}>
							<div className="card">
                            <p className="suggesting">We suggest you this course</p>
								<div className="difficulty" id={data.difficulty} >
									{data.difficulty}
								</div>
								<div className="card_image">
									<img className="cardimg" alt="dataimg" src={data.image} />
								</div>
								<div className="card_content">
									<h3 className="headcard">{data.type} - {data.duration} minutes</h3>
									<h2 className="card_title">{data.title}</h2>
									<p className="card_text">{data.description}</p>
									<p className="iconskill">
										<img className="improveskill" alt="improveskill" src="https://image.flaticon.com/icons/svg/1420/1420033.svg"/>
										<br />{data.skill}</p>
										<div className="starRate">
										<Rating className="starRate"
											initialRating={data.rate}
											readonly={true}
  											emptySymbol={<i className="far fa-star" />}
											fullSymbol={<i className="fas fa-star" />}
											/><p className="star">{data.rate}/5</p>
										</div>
									<button className="btn card_btn">Read More</button>
									<input placeholder="  Commentez cette article" />
								</div>
							</div>
						</li>
					))}
				</ul>
			</div>
        )
    }
}

export default SuggestCourse;