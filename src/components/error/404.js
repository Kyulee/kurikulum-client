import React from 'react';
import { Link } from 'react-router-dom';

const Error404 = () => (
	<div>
		<img
			alt="errorimg"
			src="https://image.flaticon.com/icons/svg/1794/1794725.svg"
			style={{ width: 100, height: 100, display: 'block', margin: 'auto', position: 'relative' }}
		/>
		<h1>Sorry, this page does not exist.</h1>
		<center>
			<Link to="/">Return to Home Page</Link>
		</center>
	</div>
);

export default Error404;
