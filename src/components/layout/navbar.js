import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../stylesheets/layout/navbar.scss';
import 'bootstrap/dist/css/bootstrap.css';
import axios from 'axios';

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      logged: false,
      username: "",
      isToggleOn: true,
      avatar: "https://bit.ly/37As7Oi"
     };

    this.handleClick = this.handleClick.bind(this);
    this.logout = this.logout.bind(this)
  }

  handleClick() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  }
  componentDidMount(){
    const header = {
      'x-auth-token': localStorage.getItem('token')
    }
    
    if(header["x-auth-token"]){
      axios.get("http://localhost:8666/user",
      { headers: header})
      .then(res => {
        this.setState({
          name: res.data.username,
          avatar: res.data.avatar
        });
      })
    }
  }
  
  logout (){
    localStorage.removeItem('token')
    this.props.history.push("/login");
  }

  render() {
    const { avatar } = this.state;
    const userDropdown = (
      <div className='my-2 my-lg-0'>
        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
          <li className="nav-item dropdown">
            <a href="!#" className="nav-link dropdown-toggle" data-toggle="dropdown"  role="button" aria-haspopup="true" aria-expanded="false">
              { avatar ? <img alt='avatar' src={ avatar } className='nav-avatar' ></img> : <i className="fa fa-user fa-fw"></i> }
            </a>
            <div className="dropdown-menu dropdown-menu-right">
              <Link className="dropdown-item" to="/user/dashboard/myprofile">See mon profil</Link>
              <Link className="dropdown-item" to="/user/dashboard/bookmark">Bookmark</Link>
              <Link className="dropdown-item" to="/user/dashboard/myprofile">Settings</Link>
              <div className="dropdown-divider"></div>
              <Link className="dropdown-item" to="/login" onClick={this.logout}><i className="fas fa-sign-out-alt"></i> Log Out</Link>
            </div>
          </li>
        </ul>
      </div>
    )
    const userLinks = (
      <>
        <li className="nav-item">
          <Link className="nav-link nav-link-left" to="/allcourses">All courses</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link nav-link-left" to="/searchcourse">Search a course</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link nav-link-left" to="/uploadcourse">Create a course</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link nav-link-left" to="/contactus">Contact us</Link>
        </li>
      </>
    )
    const offlineLinks = (
      <>
        <li className="nav-item">
          <Link className="nav-link nav-link-left" to="/login">Log In</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link nav-link-left" to="/register">Join us !</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link nav-link-left" to="/aboutus">About us</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link nav-link-left" to="/contactus">Contact us</Link>
        </li>
      </>
  )
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light row">
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon col"></span>
          </button>
          <Link className='navbar-brand col' to='/'>
            <img className='navimg' src="https://image.flaticon.com/icons/svg/2223/2223881.svg" alt='logo' />
          </Link>
        
          <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul className="navbar-nav mr-auto mt-3 mt-lg-0">

              { localStorage.token ? userLinks : offlineLinks }
            </ul>
            
              { localStorage.token ? userDropdown : null }
            
            </div>
        </nav>
      </div>
    )
  }
}

export default Navbar