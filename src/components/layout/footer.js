import React, { Component } from 'react';
import '../stylesheets/layout/footer.scss';

class Footer extends Component {
    render () {
        return(
            <div className="footer">
                <img className="footerimg" src="https://image.flaticon.com/icons/svg/648/648167.svg" alt="footerimg"></img>
            </div>
        )
    }
}

export default Footer;