import React, { useState } from 'react';
import LastCourses from './lastcourses';
import '../stylesheets/homepage/homepage.scss';
import useAuth from '../login/useAuth.js';
import Image from '../images/kurikulumhome.png';

function Home(props) {
	const [ user, setUser ] = useState({});
	const { getUserInfo } = useAuth();

	getUserInfo(setUser);
	return (
		<div>
			<img className="home" src={Image} alt="home" />
			<br />
			<h1>Hello {user.username} !</h1>
            <hr />
			{/* <LastCourses /> */}
		</div>
	);
}

export default Home;
