import React, {Component} from 'react';
import Rating from 'react-rating';

class LastCourses extends Component {
    constructor(props) {
		super(props);
		this.state = {
			courses: [
				{
                    id: 1,
                    image: require('../images/coding.png'),
					type: 'Article',
					duration: '20',
					author: 'Yuli',
					title: 'Bases de C',
					description: 'Voici les bases du C.',
					skill: 'C, Programming',
					difficulty: 'Beginner',
					createdAt: '10/04/2018',
                    lastUpdate: '12/12/2019',
                    rate: '4'
				},
				{
                    id: 2,
                    image: require('../images/marketing.png'),
					type: 'Video',
					duration: '60',
					author: 'Ben',
					title: 'Marketing en 1 heure',
					description: 'Marketing, Vendre.',
					skill: 'Marketing, Selling',
					difficulty: 'Intermediate',
					createdAt: '05/10/2015',
                    lastUpdate: '12/12/2019',
                    rate: '4.5'
				},
				{
                    id: 5,
                    image: require('../images/coding.png'),
					type: 'Podcast',
					duration: '130',
					author: 'Emy',
					title: 'Python',
					description: 'Bases du python',
					skill: 'Python, Programming',
					difficulty: 'Beginner',
					createdAt: '15/11/2011',
                    lastUpdate: '12/12/2019',
                    rate: '4.5'
				}
			]
		};
	}
    render () {
        return (
            <div>
            <h1>Last course updated</h1>
            <ul className="cards">
            {this.state.courses.map((data) => (
                <li className="cards_item" key={data.id}>
                    <div className="card">
                        <div className="difficulty" id={data.difficulty} >
                            {data.difficulty}
                        </div>
                        <div className="card_image">
                            <img className="cardimg" alt="dataimg" src={data.image} />
                        </div>
                        <div className="card_content">
                            <h3 className="headcard">{data.type} - {data.duration} minutes</h3>
                            <h2 className="card_title">{data.title}</h2>
                            <p className="card_text">{data.description}</p>
                            <p className="iconskill">
                                <img className="improveskill" alt="improveskill" src="https://image.flaticon.com/icons/svg/1420/1420033.svg"/>
                                <br />{data.skill}</p>
                                <div className="starRate">
                                <Rating className="starRate"
                                    initialRating={data.rate}
                                    readonly={true}
                                      emptySymbol={<i className="far fa-star" />}
                                    fullSymbol={<i className="fas fa-star" />}
                                    /><p className="star">{data.rate}/5</p>
                                </div>
                            <button className="btn card_btn">Read More</button>
                            <input placeholder="  Commentez cette article" />
                        </div>
                    </div>
                </li>
            ))}
        </ul>
        </div>
        )
    }
}

export default LastCourses;