import React, { Component } from 'react';
import Chart from 'react-apexcharts';
import '../stylesheets/dashboardUser/dashboardUser.scss';

class Progress extends Component {
	constructor(props) {
		super(props);
		this.state = {
			options: {
				chart: {
					id: 'basic-bar'
				},
				xaxis: {
					categories: [
						'Commerce',
						'Marketing',
						'Programming',
						'E-commerce',
						'Law',
						'Economic',
						'Management',
						'Organization'
					]
				}
            },
            title: {
                text: 'Skill tree',
                align: 'left',
                margin: 10,
                offsetX: 0,
                offsetY: 0,
                floating: false
              },
			series: [
				{
					name: 'Skill point',
					data: [ 3, 4, 4.5, 5, 4, 6, 7, 9 ]
				}
			]
		};
	}
	render() {
		return (
			<div>
				<img
					className="progressimg"
					alt="progressimg"
					src="https://image.flaticon.com/icons/png/512/2038/2038055.png"
				/>
				<h1>My progress</h1>
				<div className="mixed-chart">
                    <Chart options={this.state.options}
                     series={this.state.series}
                      type="radar"
                       width="650"
                       text={this.state.title}
                       align="center" />
				</div>
				<div />
			</div>
		);
	}
}

export default Progress;
