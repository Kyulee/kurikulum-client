import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

class MyUpload extends Component {
	constructor(props) {
		super(props);
		this.state = {
			courses: [
				{
					id: 2,
					image: require('../images/marketing.png'),
                    type: 'Video',
                    categorie: 'Marketing',
					duration: '60',
					author: 'Ben',
					title: 'Marketing en 1 heure',
					description: 'Marketing, Vendre.',
					skill: 'Marketing, Selling',
                    difficulty: 'Intermediate',
                    done: '78'
				}]
		};
	}
	render() {
		return (
			<div className="mycourses">
				<img
					className="mycourseimg"
					alt="mycourseimg"
					src="https://image.flaticon.com/icons/png/512/2038/2038509.png"
				/>
				<h1>Course added</h1>
				<br />
				<Table striped bordered hover responsive="sm">
					<thead className="thead-light">
						<tr>
                            <th>Type</th>
							<th>Nom du cours</th>
							<th>Theme</th>
							<th>Compétences améliorés</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                        {this.state.courses.map(data=> <tr className="cours" key={data.id}>
							<td>{data.type}</td>
                            <td>{data.title}</td>
							<td>{data.categorie}</td>
                            <td>{data.skill}</td>
							<td>
								<button className="detail">See</button>
                                <button className="delete" >Delete</button>
							</td>
						</tr>)}
					</tbody>
				</Table>
			</div>
        )
    }
}

export default MyUpload;