import React, { Component } from 'react';
import { Table, ProgressBar } from 'react-bootstrap';

class Mycourse extends Component {
	constructor(props) {
		super(props);
		this.state = {
			courses: [{
					id: 1,
					image: require('../images/coding.png'),
                    type: 'Article',
                    categorie: 'Programming',
					duration: '20',
					author: 'Yuli',
					title: 'Bases de C',
					description: 'Voici les bases du C.',
					skill: 'C, Programming',
                    difficulty: 'Beginner',
					done: '100'
				},
				{
					id: 2,
					image: require('../images/marketing.png'),
                    type: 'Video',
                    categorie: 'Marketing',
					duration: '60',
					author: 'Ben',
					title: 'Marketing en 1 heure',
					description: 'Marketing, Vendre.',
					skill: 'Marketing, Selling',
                    difficulty: 'Intermediate',
                    done: '78'
				},
				{
					id: 3,
					image: require('../images/ecommerce.png'),
                    type: 'Podcast',
                    categorie: 'E-commerce',
					duration: '10',
					author: 'Philippe',
					title: 'Référencement web',
					description: 'Know all about Google Trends ',
					skill: 'SEO, SMO ',
                    difficulty: 'Advanced',
                    done: '42'
				},
				{
					id: 4,
					image: require('../images/coding.png'),
                    type: 'Article',
                    categorie: 'Programming',
					duration: '15',
					author: 'Mark',
					title: 'React',
					description: 'How to display your array ?',
					skill: 'React, Programming',
                    difficulty: 'Advanced',
                    done: '29'
				},
				{
					id: 5,
					image: require('../images/coding.png'),
                    type: 'Podcast',
                    categorie: 'Programming',
					duration: '130',
					author: 'Emy',
					title: 'Python',
					description: 'Bases du python',
					skill: 'Python, Programming',
                    difficulty: 'Beginner',
                    done: '10'
				}
			]
		};
	}
	render() {
		return (
			<div className="mycourses">
				<img
					className="mycourseimg"
					alt="mycourseimg"
					src="https://image.flaticon.com/icons/png/512/2038/2038116.png"
				/>
				<h1>All my courses</h1>
				<br />
				<Table striped bordered hover responsive="sm">
					<thead className="thead-light">
						<tr>
                            <th>Type</th>
							<th>Nom du cours</th>
							<th>Theme</th>
							<th>Compétences améliorés</th>
							<th>% d'effectués</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                        {this.state.courses.map(data=> <tr className="cours" key={data.id}>
							<td>{data.type}</td>
                            <td>{data.title}</td>
							<td>{data.categorie}</td>
                            <td>{data.skill}</td>
							<td>
								<ProgressBar variant="success" animated now={data.done} /> {data.done} %
							</td>
							<td>
								<button className="detail">See</button>
								{ data.rate === '100' ? <button className="rate">Rate</button> : null}
							</td>
						</tr>)}
					</tbody>
				</Table>
			</div>
		);
	}
}

export default Mycourse;
