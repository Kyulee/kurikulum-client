import React, { useState } from 'react';
import { Card } from 'react-bootstrap';
import '../stylesheets/dashboardUser/myprofile.scss';
import useAuth from "../login/useAuth.js";

function Myprofile(props) {
    const [user, setUser] = useState({})
    const { isUserAuth, getUserInfo } = useAuth();
    
    if (!isUserAuth()) {
      console.log("Not logged in !");
      props.history.push("/auth/login");
    }
    else {
      getUserInfo(setUser)
    }
        return (
            <div className="dashboard-profil">
                <h1>Welcome back, {user.username} !</h1>
                <div className="row">
                    <button className="col-6" onClick={() => {props.history.push('/user/dashboard')}}>
                        Dashboard
                    </button>
                    <button className="col-6" onClick={() => {props.history.push('/user/dashboard/myprofile')}}>
                        My profile
                    </button>
                </div>
                <Card className="myprofilcard">
                    <Card.Img className="profilavatar" variant="top" src="https://bit.ly/37As7Oi" />
                    <Card.Body>
                        <Card.Title>Username :</Card.Title><Card.Text>{user.username}</Card.Text>
                        <Card.Title>E-mail :</Card.Title><Card.Text>{user.email}</Card.Text>
                        <div className='col-sm-12'>
                            <div className='row'>
                                <div className='col-sm-12'>
                                        <button className='user-btn btn-info col-sm-4' onClick={() => { props.history.push("/editpassword") }}>
                                            Change password
                                        </button>
                                        <button className='user-btn btn-info col-sm-4' onClick={() => { props.history.push("/editProfile") }}>
                                            Modify profile
                                        </button>
                                        <button className='user-btn btn-warn col-sm-4' onClick={alert}>
                                            Delete profile
                                        </button>
                                </div>
                            </div>
                        </div>
                    </Card.Body>
                </Card>
            </div>
        )
    }

export default Myprofile;