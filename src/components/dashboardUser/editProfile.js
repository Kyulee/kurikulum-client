import React, { Component } from 'react';
import axios from 'axios';

class EditProfile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null,
            username: "",
            email: "",
            password: ""
        };
    }

    componentDidMount() {
        this.setState({
            username: this.state.username,
            newUsername: this.state.username,
            email: this.state.email,
            newEmail: this.state.email,
            password: this.state.password,
            newPassword: this.state.password
        });

        const header = {
            'x-auth-token': localStorage.getItem('token')
        }
        axios.get("http://localhost:8666/user",
            { headers: header},
        ).then(res => {
              this.setState({ user: res.data });
          })
          .catch(err => {
              console.error(err);
          })
    }

    handleChange = (event) => {
        this.setState({
          [event.target.name]: event.target.value
        });
    }



    handleEditUser = e => {
        e.preventDefault();

        const header = {
            'x-auth-token': localStorage.getItem('token')
        }

        var editUser = {
            username: this.state.newUsername,
            email: this.state.newEmail,
            password: this.state.newPassword,
            id : this.state.user._id
        };

        axios.put("http://localhost:8666/user" + editUser.id, editUser, { headers: header })
            .then(res => {
                alert('Votre profil a été modifié');
                this.setState({ user: res.data });
                this.props.history.push('/user/dahsboard/myprofile');
            })
            .catch(err => {
                console.error(err.response);
            })
    }

    render () {
        // var {
        //     user
        // } = this.state
        const { history } = this.props;
        return (
            <div className="container">
           <div className= 'row'>
               <form className='col-12 user-form login-form' onSubmit={ this.handleEditUser }>
                   <div className='title'>
                       <h1>Modify your profile</h1>
                   </div>
                   <div className='group user-inputs'>
                       <input type='text' placeholder='username' className='input-username' onChange={ this.handleChange }></input>
                       <span className="highlight"></span>
                       <span className="bar"></span>
                       <label>Your new username</label>
                   </div>
                   <div className='group user-inputs'>
                       <input type='email' name='newEmail' className='input-email' placeholder='email' onChange={ this.handleChange }></input>
                       <span className="highlight"></span>
                       <span className="bar"></span>
                       <label>Your new email</label>
                   </div>
                   {/* <div className='group user-inputs'>
                       <input type='password' name='newPassword' className='input-password' placeholder='Mot de Passe' onChange={ this.handleChange }></input>
                       <span className="highlight"></span>
                       <span className="bar"></span>
                       <label>Nouveau mot de passe</label>
                   </div> */}
                   <div className='row'>
                   <button className="btn-info submit col">Submit</button>
                           <button className="btn-warn col" onClick={() => { history.push("/user/dashboard/myprofile") }}>
                               Cancel
                           </button>
                   </div>
               </form>
           </div>
       </div>
        )
    }
}

export default EditProfile;