import React, { Component } from 'react';

class EditPassword extends Component {
    render () {
        const { history } = this.props;
        return (
            <div className="container">
            <div className= 'row'>
                <form className='col-12 user-form login-form' onSubmit={ this.handleEditUser }>
                    <div className='title'>
                        <h1>Change your password</h1>
                    </div>
                    <div className='group user-inputs'>
                        <input type='password' placeholder='current password' className='input-oldpassword' onChange={ this.handleChange }></input>
                        <span className="highlight"></span>
                        <span className="bar"></span>
                        <label>Your password</label>
                    </div>
                    <div className='group user-inputs'>
                        <input type='password' name='newpassword' className='input-password' placeholder='new password' onChange={ this.handleChange }></input>
                        <span className="highlight"></span>
                        <span className="bar"></span>
                        <label>Your new password</label>
                    </div>
                    <div className='row'>
                    <button className="btn-info submit col">Submit</button>
                            <button className="btn-warn col" onClick={() => { history.push("/user/dashboard/myprofile") }}>
                                Back
                            </button>
                    </div>
                </form>
            </div>
        </div>
        )
    }
}

export default EditPassword;