import React, { useState } from 'react';
import { Table, Modal, Button } from 'react-bootstrap';

function Bookmark () {
        const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
        return (
            <div>
                <img className="bookmarkimg" alt="bookmarkimg" 
                src="https://image.flaticon.com/icons/png/512/2038/2038200.png" />
                <h1>Bookmark</h1>
                <br />
                <Table striped bordered hover responsive="sm">
                    <thead className="thead-light">
                        <tr>
                            <th>Nom du cours</th>
                            <th>Theme</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Bases du Marketing</td>
                            <td>Marketing</td>
                            <td><button className="detail">See</button> 
                            <button className="delete" onClick={handleShow}>Delete</button></td>
                        </tr>
                        <tr>
                            <td>C'est quoi une Marketplace ?</td>
                            <td>E-Commerce</td>
                            <td><button className="detail">See</button> 
                            <button className="delete" onClick={handleShow}>Delete</button></td>
                        </tr>
                        <tr>
                            <td>Améliorer vos compétences en Python</td>
                            <td>Programmation</td>
                            <td><button className="detail">See</button> 
                            <button className="delete" onClick={handleShow}>Delete</button></td>
                        </tr>
                        <tr>
                            <td>Comment travailler à la maison ?</td>
                            <td>Organisation</td>
                            <td><button className="detail">See</button> 
                            <button className="delete" onClick={handleShow}>Delete</button></td>
                        </tr>

                        <Modal show={show} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Vous êtes sur de vouloir supprimer ce cours ?</Modal.Title>
                            </Modal.Header>
                            <Modal.Footer>
                                <Button variant="primary" onClick={handleClose}>
                                    Oui
                                </Button>
                                <Button variant="secondary" onClick={handleClose}>
                                    Annuler
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </tbody>
                </Table>
            </div>
        )
    }

export default Bookmark;