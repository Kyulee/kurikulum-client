import React, { useState } from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import '../stylesheets/dashboardUser/dashboardUser.scss';
import Bookmark from './bookmark.js';
import Mycourse from './mycourse.js';
import Progress from './progress.js';
import MyUpload from './myupload.js';
import useAuth from "../login/useAuth.js";

function Dashboard(props) {
    const [user, setUser] = useState({})
    const { isUserAuth, getUserInfo } = useAuth();
    
    if (!isUserAuth()) {
      console.log("Not logged in !");
      props.history.push("/auth/login");
    }
    else {
      getUserInfo(setUser)
    }
        return (
            <div className="dashboard-user">
                <h1>Welcome back, {user.username} !</h1>
                <div className="row">
                    <button className="col-6" onClick={() => { props.history.push("/user/dashboard") }}>
                        Dashboard
                </button>
                    <button className="col-6" onClick={() => { props.history.push("/user/dashboard/myprofile") }}>
                        My profile
                </button>
                </div>
                <div className="tabs">
                    <Tabs defaultActiveKey="mycourse" id="uncontrolled-tab-example">
                    <Tab eventKey="mycourse" title="All my courses">
                        <Mycourse />
                    </Tab>
                    <Tab eventKey="bookmark" title="Bookmark">
                        <Bookmark />
                    </Tab>
                    <Tab eventKey="progress" title="My progress">
                        <Progress />
                    </Tab>
                    <Tab eventKey="myupload" title="Course added">
                        <MyUpload />
                    </Tab>
                </Tabs>
                </div>
            </div>
        )
    }


export default Dashboard;