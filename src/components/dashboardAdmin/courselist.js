import React, { Component } from 'react';
import { Table, Pagination, Modal, Button } from 'react-bootstrap';

class CourseList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			courses: [
				{
					id: 1,
					image: require('../images/coding.png'),
					type: 'Article',
					duration: '20',
					author: 'Yuli',
					title: 'Bases de C',
					description: 'Voici les bases du C.',
					skill: 'C, Programming',
					difficulty: 'Beginner',
					createdAt: '10/04/2018',
					lastUpdate: '12/12/2019'
				},
				{
					id: 2,
					image: require('../images/marketing.png'),
					type: 'Video',
					duration: '60',
					author: 'Ben',
					title: 'Marketing en 1 heure',
					description: 'Marketing, Vendre.',
					skill: 'Marketing, Selling',
					difficulty: 'Intermediate',
					createdAt: '05/10/2015',
					lastUpdate: '12/12/2019'
				},
				{
					id: 3,
					image: require('../images/ecommerce.png'),
					type: 'Podcast',
					duration: '10',
					author: 'Philippe',
					title: 'Référencement web',
					description: 'Know all about Google Trends ',
					skill: 'SEO, SMO ',
					difficulty: 'Advanced',
					createdAt: '05/07/2008',
					lastUpdate: '12/12/2019'
				},
				{
					id: 4,
					image: require('../images/coding.png'),
					type: 'Article',
					duration: '15',
					author: 'Mark',
					title: 'React',
					description: 'How to display your array ?',
					skill: 'React, Programming',
					difficulty: 'Advanced',
					createdAt: '29/01/2006',
					lastUpdate: '12/12/2019'
				},
				{
					id: 5,
					image: require('../images/coding.png'),
					type: 'Podcast',
					duration: '130',
					author: 'Emy',
					title: 'Python',
					description: 'Bases du python',
					skill: 'Python, Programming',
					difficulty: 'Beginner',
					createdAt: '15/11/2011',
					lastUpdate: '12/12/2019'
				}
			]
		};
	}
	render() {
    const modal = (
      <>
        									<Modal.Dialog>
										<Modal.Header closeButton>
											<Modal.Title>Supprimer ce cours ?</Modal.Title>
										</Modal.Header>
										<Modal.Body>
											<p>T'es sur frero ?</p>
										</Modal.Body>
										<Modal.Footer>
											<Button variant="secondary">Annuler</Button>
											<Button variant="primary">Oui, je le veux !</Button>
										</Modal.Footer>
									</Modal.Dialog>
      </>

    )
		let active = 1;
		const courses = [];
		for (let number = 1; number <= 3; number++) {
			courses.push(
				<Pagination.Item key={number} active={number === active}>
					{number}
				</Pagination.Item>
			);
		}
		return (
			<div className="courselist">
				<Table striped bordered hover size="sm" responsive="sm">
					<thead className="thead-dark">
						<tr>
							<th>
								<label htmlFor="scales">Select</label>
								<input type="checkbox" name="checkall" value="checkall" />
							</th>
							<th>Id</th>
							<th>Type</th>
							<th>Title</th>
							<th>Author</th>
							<th>Duration</th>
							<th>Difficulty</th>
							<th>Crée le</th>
							<th>Last update</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{this.state.courses.map((data) => (
							<tr className="cours" key={data.id}>
								<td>
									<input type="checkbox" name="checkall" value="checkall" />
								</td>
								<td>{data.id}</td>
								<td>{data.type}</td>
								<td>{data.title}</td>
								<td>{data.author}</td>
								<td>{data.duration} min</td>
								<td>{data.difficulty}</td>
								<td>{data.createdAt}</td>
								<td>{data.lastUpdate}</td>
								<th>
									<button className="detail">Voir</button>
									<button className="delete" >Supprimer</button>

								</th>
							</tr>
						))}
					</tbody>
				</Table>
				<Pagination size="sm">{courses}</Pagination>
			</div>
		);
	}
}

export default CourseList;
