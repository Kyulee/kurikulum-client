import React, { Component } from 'react';
import ChartVisit from './charts/chartvisit';
import ChartDay from './charts/chartday';
import LastCourseUp from './charts/lastcourseup';
import PieCourse from './charts/piecourse';
import LastRegister from './charts/lastregister';

import '../stylesheets/dashboardAdmin/stats.scss';

class Stats extends Component {
	render() {
		return (
			<div>
				<h2>Tableau de bord</h2>
                <br />
				<div className="row">
					<div className="col-sm-4 col-xs-12">
						<ChartVisit />
					</div>
					<div className="col-sm-4 col-xs-12">
						<ChartDay />
					</div>
                    <div className="col-sm-4 col-xs-12">
                        <PieCourse />
                    </div>
				</div>
                <br />
				<div className="row-12 lastcourse chart">
					<LastCourseUp />
				</div>
                <br />
                <div className="row-12">
                    <LastRegister />
                </div>
			</div>
		);
	}
}

export default Stats;
