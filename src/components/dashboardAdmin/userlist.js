import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import '../stylesheets/dashboardAdmin/userlist.scss';
import { Pagination } from 'react-bootstrap';

class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = {
        users: [{
            id: 1,
            username: 'userAnonymous1',
            email: 'First@hello.com',
            role: 'user',
            timespending: '150',
            description: 'Hello, Im the first user of this website'
        }, {
            id: 2,
            username: 'test',
            email: 'testtest@hello.com',
            role: 'user',
            timespending: '70',
            description: 'Hello, Im a test'
        }, {
            id: 3,
            username: 'Default',
            email: 'Default@hello.com',
            role: 'user',
            timespending: '0',
            description: 'Hello, Im new on this website'
        }, {
            id: 4,
            username: 'BenGecko',
            email: 'Ben@Gecko.com',
            role: 'admin',
            timespending: '150',
            description: 'Gecko'
        }, {
            id: 5,
            username: 'kopo',
            email: 'kopo@kopo.com',
            role: 'user',
            timespending: '42',
            description: 'Kopo ?'
        }, {
          id: 6,
          username: 'Julie',
          email: 'julie@gecko.com',
          role: 'admin',
          timespending: '10',
          description: 'Gecko'
      }],
    };
}

    render() {
      let active = 1
      const users = []
      for (let number = 1; number <= 3; number++) {
        users.push(
          <Pagination.Item key={number} active={number === active }>
            {number}
          </Pagination.Item>,
        );
      }
        return (
          <div>
            <div>
            <Table striped bordered hover size="sm" responsive="sm">
  <thead className="thead-dark">
    <tr>
      <th><label htmlFor="scales">Select</label><input type="checkbox" name="checkall" value="checkall" /></th>
      <th>Id</th>
      <th>Username</th>
      <th>Email</th>
      <th>Role</th>
      <th>Time spending</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  {this.state.users.map(data=> <tr className="userlist" key={data.id}>
      <td><input type="checkbox" name="checkall" value="checkall" /></td>
      <td>{data.id}</td>
      <td>{data.username}</td>
        <td>{data.email}</td>
      <td>{data.role}</td>
      <td>{data.timespending} minutes</td>
      <td className="row buttonuserlist">
        <button className="col detail">Voir</button>
        <button className="col modify">Modifier</button>
        <button className="col delete" >Supprimer</button>
        </td>
    </tr>)}
  </tbody>
</Table>
</div>
<Pagination size="sm">{users}</Pagination>
</div>
        )
    }
}

export default UserList;