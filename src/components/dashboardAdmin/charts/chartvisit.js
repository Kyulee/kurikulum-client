import React, { Component } from 'react';
import ReactApexChart from 'react-apexcharts';


class ChartVisit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                plotOptions: {
                    bar: {
                        vertical: true,
                        dataLabels: {
                          position: 'top',
                        },
                      }
                    },
                    dataLabels: {
                      enabled: true,
                      offsetX: 0,
                      style: {
                        fontSize: '12px',
                        colors: ['#fff']
                      }
                    },
                    stroke: {
                      show: true,
                      width: 1,
                      colors: ['#fff']
                    },
                xaxis: {
                    categories: [
                      'Juillet','Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'
                    ],
                }
            },
            series: [{
                name: "Visiteur non connectés",
                data: [22, 43, 21, 12, 20, 100]
              }, {
                name: "Visiteurs connectés", 
                data: [13, 44, 32, 29, 49, 80]
              }],
        }
        
    }
    render () {
        return (
            <div id="chart">
              <h1>Nombre de visite</h1>
            <ReactApexChart options={this.state.options} series={this.state.series} type="bar" height="350" />
          </div>
        )
    }
}

export default ChartVisit;