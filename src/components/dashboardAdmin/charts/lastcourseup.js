import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

class LastCourseUp extends Component {
    constructor(props) {
		super(props);
		this.state = {
			courses: [
				{
					id: 1,
					type: 'Article',
					duration: '20',
					author: 'Yuli',
					title: 'Bases de C',
					description: 'Voici les bases du C.',
					skill: 'C, Programming',
					difficulty: 'Beginner',
					createdAt: '10/04/2018',
					lastUpdate: '12/12/2019'
				},
				{
					id: 2,
					type: 'Video',
					duration: '60',
					author: 'Ben',
					title: 'Marketing en 1 heure',
					description: 'Marketing, Vendre.',
					skill: 'Marketing, Selling',
					difficulty: 'Intermediate',
					createdAt: '05/10/2015',
					lastUpdate: '12/12/2019'
				},
				{
					id: 5,
					type: 'Podcast',
					duration: '130',
					author: 'Emy',
					title: 'Python',
					description: 'Bases du python',
					skill: 'Python, Programming',
					difficulty: 'Beginner',
					createdAt: '15/11/2011',
					lastUpdate: '12/12/2019'
				}
			]
		};
	}
    render () {
        return (
            <div className="courselist">
                <h1>Derniers cours en ligne</h1>
            <Table striped bordered hover size="sm" responsive="sm">
                <thead className="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Type</th>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Duration</th>
                        <th>Difficulty</th>
                        <th>Crée le</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.courses.map((data) => (
                        <tr className="cours" key={data.id}>
                            <td>{data.id}</td>
                            <td>{data.type}</td>
                            <td>{data.title}</td>
                            <td>{data.author}</td>
                            <td>{data.duration} min</td>
                            <td>{data.difficulty}</td>
                            <td>{data.createdAt}</td>
                            <th>
                                <button className="detail">Voir</button>
                            </th>
                        </tr>
                    ))}
                </tbody>
            </Table>
            </div>
        )
    }
}

export default LastCourseUp;