import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

class LastRegister extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [{
                id: 4,
                username: 'BenGecko',
                email: 'Ben@Gecko.com',
                role: 'admin',
                timespending: '150',
                description: 'Gecko'
            }, {
                id: 5,
                username: 'kopo',
                email: 'kopo@kopo.com',
                role: 'user',
                timespending: '42',
                description: 'Kopo ?'
            }, {
              id: 6,
              username: 'Julie',
              email: 'julie@gecko.com',
              role: 'admin',
              timespending: '10',
              description: 'Gecko'
          }],
        };
    }
	render() {
		return (
			<div>
				<h1>Derniers inscrits</h1>
				<Table striped bordered hover size="sm" responsive="sm">
					<thead className="thead-dark">
						<tr>
							<th>Id</th>
							<th>Username</th>
                            <th>Email</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{this.state.users.map((data) => (
							<tr className="userlist" key={data.id}>
								<td>{data.id}</td>
								<td>{data.username}</td>
                                <td>{data.email}</td>
								<td className="buttonuserlist">
									<button className="detail">Voir</button>
								</td>
							</tr>
						))}
					</tbody>
				</Table>
			</div>
		);
	}
}

export default LastRegister;
