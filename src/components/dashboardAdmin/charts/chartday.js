import React, { Component } from 'react';
import ReactApexChart from 'react-apexcharts';

class ChartDay extends Component {
    constructor(props) {
        super(props);

        this.state = {
          options: {
            chart: {
                  zoom: {
                      enabled: false
                  }
              },
              dataLabels: {
                  enabled: false
              },
              stroke: {
                  curve: 'straight'
              },
              grid: {
                  row: {
                      colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                      opacity: 0.5
                  },
              },
              xaxis: {
                  categories: ['12h', '13h', '14h', '15h', '16h', '17h', '18h', '19h', '20h'],
              }
          },
          series: [{
              name: "Visiteurs",
              data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
          }],
        }
      }
    render () {
        return (
            <div id="chart">
                <h1>Visite du jour</h1>
            <ReactApexChart options={this.state.options} series={this.state.series} type="line" height="350" />
          </div>
        )
    }
}

export default ChartDay;