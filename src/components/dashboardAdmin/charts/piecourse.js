import React, { Component } from 'react';
import ReactApexChart from 'react-apexcharts';

class PieCourse extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                labels: ['Marketing', 'E-Commerce', 'Referencement', 'Programmation', 'Organisation'],
                // responsive: [{
                //     breakpoint: 480,
                //     options: {
                //         chart: {
                //             width: 220
                //         },
                //     }
                // }]
            },
            series: [44, 55, 63, 83, 12] 
        }
    }
    render () {
        return (
            <div id="responsive-chart">
                <h1>Part of course</h1>
            <ReactApexChart options={this.state.options} series={this.state.series} type="donut" width="380" />
          </div>
            
        )
    }
}

export default PieCourse;