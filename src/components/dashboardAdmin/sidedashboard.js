import React, { Component } from 'react';
import UserList from './userlist';
import CourseList from './courselist';
import Stats from './stats';
import { Tab, Nav } from 'react-bootstrap';
import '../stylesheets/dashboardAdmin/dashboardAdmin.scss';

class SideDashboard extends Component {
    render (){
        return (
            <div>
                <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                <div className="row sidenavbar">
                <div className="col-2 adminnavbar">
				<Nav defaultActiveKey="/home" className="flex-column" style={{width: 'auto'}}>
                    
					<h3>Dashboard Admin</h3>
                    <br />
                    <img className="adminimg" alt="adminimg"
                    src="https://image.flaticon.com/icons/svg/2354/2354004.svg" />
                    <br />
                    <Nav.Item>
					<Nav.Link eventKey="first">Dashboard</Nav.Link>
                    </Nav.Item>
                    <br />
                    <Nav.Item>
					<Nav.Link eventKey="second">User</Nav.Link>
                    </Nav.Item>
                    <br />
                    <Nav.Item>
					<Nav.Link eventKey="third">Ressources</Nav.Link>
                    </Nav.Item>
				</Nav>
                </div>
            <div className="col-9 tableau">
				<Tab.Content>
					<Tab.Pane eventKey="first">
						<Stats />
					</Tab.Pane>
					<Tab.Pane eventKey="second">
						<UserList />
					</Tab.Pane>
					<Tab.Pane eventKey="third">
						<CourseList />
					</Tab.Pane>
				</Tab.Content>
                </div>
                </div>
            </Tab.Container>
            </div>
        )
    }
}

export default SideDashboard;