import React, { useState } from 'react';
import useAuth from '../login/useAuth.js';
import SideDashboard from './sidedashboard';
import '../stylesheets/dashboardAdmin/dashboardAdmin.scss';

function DashboardAdmin(props) {
	const [ user, setUser ] = useState({});
	const { isUserAuth, getUserInfo } = useAuth();

	if (!isUserAuth()) {
		console.log('Not logged in !');
		props.history.push('/auth/login');
	} else {
		getUserInfo(setUser);
	}
	return (
		<div className="dashboardadmin">
                <h1> Welcome, Master {user.username} !</h1>
				<br />
				<SideDashboard />
        </div>
	);
}

export default DashboardAdmin;
