import React, { Component } from 'react';
import Image from '../images/kurikulumabout.png';
import '../stylesheets/support/contactpage.scss';

class AboutUs extends Component {
	render() {
		return (
			<div>
				<img className="home" src={Image} alt="home" />
				<div>
				<hr />
				<h2>About Kurikulum</h2>
				<hr />
				<br />
				<p className="about-text">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
					et dolore magna aliqua. Nibh sit amet commodo nulla facilisi nullam vehicula ipsum a. Ut morbi
					tincidunt augue interdum velit euismod in pellentesque massa. Nulla porttitor massa id neque aliquam
					vestibulum morbi. Sed tempus urna et pharetra pharetra. Urna molestie at elementum eu facilisis sed
					odio morbi. Eu volutpat odio facilisis mauris. Magna sit amet purus gravida quis. Facilisi nullam
					vehicula ipsum a arcu. Nisi scelerisque eu ultrices vitae. Maecenas volutpat blandit aliquam etiam
					erat. Elit eget gravida cum sociis natoque penatibus.
				</p>
				<p>
					Volutpat diam ut venenatis tellus in
					metus vulputate. Pharetra et ultrices neque ornare aenean euismod elementum nisi quis. Aliquet enim
					tortor at auctor. Scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique
					senectus. Et magnis dis parturient montes. Ultrices eros in cursus turpis massa tincidunt dui ut.
					Urna condimentum mattis pellentesque id nibh. Et molestie ac feugiat sed lectus vestibulum mattis
					ullamcorper velit. Viverra nibh cras pulvinar mattis nunc sed blandit. Nunc congue nisi vitae
					suscipit tellus mauris.
				</p>
				</div>
			</div>
		);
	}
}

export default AboutUs;
