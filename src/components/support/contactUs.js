import React, { Component } from 'react';
import { Form } from 'react-bootstrap';
import Image from '../images/contactimg.png';
import '../stylesheets/support/contactpage.scss';

class ContactUs extends Component {
	render() {
		return (
			<div>
				<img className="contact" src={Image} alt="contact" />
				<hr />
				<h1>Contact us page</h1>
				<hr />
                <br />
				<Form>
                    <div className="row">
					<Form.Group controlId="formBasicFirstname" className="col">
						<Form.Label>First name</Form.Label>
						<Form.Control type="text" placeholder="Your first name" />
					</Form.Group>
					<Form.Group controlId="formBasicLastname" className="col">
						<Form.Label>Last name</Form.Label>
						<Form.Control type="text" placeholder="Your last name" />
					</Form.Group>
                    </div>
					<Form.Group controlId="formBasicEmail">
						<Form.Label>Email address</Form.Label>
						<Form.Control type="email" placeholder="Your email" />
					</Form.Group>
					<Form.Group controlId="exampleForm.ControlTextarea1">
						<Form.Label>Subject</Form.Label>
						<Form.Control as="textarea" rows="3" placeholder="Write something.." />
					</Form.Group>
					<button className="submit">Send it</button>
				</Form>
			</div>
		);
	}
}

export default ContactUs;
