import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import axios from 'axios';

import '../stylesheets/LoginRegister/login.scss';

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: ''
		};
		this.state = { hidden: true };
		this.routeChange = this.routeChange.bind(this);
		this.toggleShow = this.toggleShow.bind(this);
	}

	toggleShow() {
		this.setState({ hidden: !this.state.hidden });
	}

	routeChange() {
		this.props.history.push('/register');
	}

	handleChange = (event) => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};

	handleSubmit = async (event) => {
		event.preventDefault();
		if (!this.state.email && !this.state.password) {
			alert('Field "email" and "password" are empty');
			return;
		}
		axios
			.post('http://localhost:8666/login', {
				email: this.state.email,
				password: this.state.password
			})
			.then((res) => {
				console.log(res.status);
				if (res.status === 200) {
					localStorage.setItem('token', res.data.token);
					toast.success('Welcome back !');
					if (res.data.role === 'user') {
						this.props.history.push('/user/dashboard');
					} else {
						this.props.history.push('/admin');
					}
				}
			})
			.catch((err) => {
				console.log(err);
				if (err.response.data.msg) {
					if (err.response.status === 500)
						toast.error('Nous sommes désolés, nous faisons face à un problème de serveur');
          else if (err.response.data.msg === 418) 
          toast.error('Your email and password did not match');
				}
			});
	};

	render() {
		return (
			<div className="container-login container">

				<div className="form-container sign-in-container login-form">
					<form onSubmit={this.handleSubmit}>
						<h1>Sign in</h1>
						<div className="group">
							<input type="text" name="email" onChange={this.handleChange} required />
							<span className="highlight" />
							<span className="bar" />
							<label>Email</label>
						</div>
						<div className="group">
							<input
								name="password"
								onChange={this.handleChange}
								type={this.state.hidden ? 'password' : 'text'}
								required
							/>
							<span className="highlight" />
							<span className="bar" />
							<label>Password</label>
							<Link to="#" onClick={this.toggleShow}>Show / Hide password</Link>
						</div>
						<button className="submit">Im ready !</button>
					</form>
				</div>

				<div className="overlay-container">
					<div className="overlay">
						<div className="overlay-panel overlay-right">
							<h1>Not a member yet ?</h1>
							<p>Sign up to be part of the adventure</p>
							<button className="ghost" name="signUp" onClick={this.routeChange}>
								Join us here
							</button>
						</div>
					</div>
				</div>
        <ToastContainer
					position="top-left"
					autoClose={4000}
					hideProgressBar={false}
					newestOnTop={false}
					closeOnClick={false}
					rtl={false}
					pauseOnVisibilityChange={false}
					draggable
					pauseOnHover={false}
				/>
			</div>
		);
	}
}
export default withRouter(Login);
