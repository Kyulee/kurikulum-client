import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


const useAuth = () => {
  const authUser = (token, cb) => {
    localStorage.setItem("token", token);
  };

  const isUserAuth = () => {
    return localStorage.getItem("token") !== null;
  };

  const unAuthUser = cb => {
    localStorage.removeItem("token");
    cb();
  };

  const getToken = () => {
    return localStorage.getItem("token");
  };

  const getUserInfo = async (setUser) => {
    axios.get(`http://localhost:8666/user`, {
        headers: {
          Authorization: getToken()
        }
      })
      .then(res => {
        console.log(res.data.data);
        setUser(res.data.data)
      })
      .catch(err => {
        console.log(err);
        toast.error("Couldn't retrieve user");
        return null;
      });
  };

  return {
    authUser,
    isUserAuth,
    unAuthUser,
    getToken,
    getUserInfo
  };
};

export default useAuth;
