import React from 'react';
import './App.css';
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom';

import Navbar from './components/layout/navbar';
import Footer from './components/layout/footer';
import Register from './components/register/register';
import Login from './components/login/login';
import Toggle from './components/toggleLogReg/toggle.js';
import Onboardprocess from './components/onboardprocess/onboard';
import AboutUs from  './components/support/aboutUs';
import ContactUs from './components/support/contactUs';

import Error404 from './components/error/404.js';

import Greeting from './components/onboardprocess/step1Greeting.js';
import Role from './components/onboardprocess/step2Role.js';
import Subject from './components/onboardprocess/step3Subject.js';
import Autoevaluate from './components/onboardprocess/step4Autoevaluate.js';
import Ending from './components/onboardprocess/step5Ending.js';

import Dashboard from './components/dashboardUser/dashboarduser.js';
import Bookmark from './components/dashboardUser/bookmark.js';
import Mycourse from './components/dashboardUser/mycourse.js';
import Myprofile from './components/dashboardUser/myprofile.js';
import Progress from './components/dashboardUser/progress.js';
import Home from './components/homepage/homepage.js';
import EditProfile from './components/dashboardUser/editProfile';
import EditPassword from './components/dashboardUser/editpassword';

import Allcourses from './components/courses/allCourses.js';
import SearchCourse from './components/courses/searchCourse.js';
import UploadCourse from './components/courses/uploadCourse.js';
import Search from './components/courses/filter.js';

import DashboardAdmin from './components/dashboardAdmin/dashboardAdmin';

function App() {
  return (
<Router>
  <Route component={Navbar} />
  <Switch>
  <Route exact path="/" component={Home} />
  <Route exact path="/login" component={Login}/>
  <Route exact path="/register" component={Register}/>
  <Route exact path="/aboutus" component={AboutUs} />
  <Route exact path="/contactus" component={ContactUs} />
  <Route restricted={true} exact path="/toggle" component={Toggle}/>
  <Route exact path="/welcome" component={Onboardprocess}/>
  <Route exact path="/welcome/step1" component={Greeting} />
  <Route exact path="/welcome/step2" component={Role} />
  <Route exact path="/welcome/step3" component={Subject} />
  <Route exact path="/welcome/step4" component={Autoevaluate} />
  <Route exact path="/welcome/step5" component={Ending} />
  <Route exact path="/user/dashboard" component={Dashboard} />
  <Route exact path="/user/dashboard/mycourse" component={Mycourse} />
  <Route exact path="/user/dashboard/myprofile" component={Myprofile} />
  <Route exact path="/user/dashboard/bookmark" component={Bookmark} />
  <Route exact path="/user/dashboard/progress" component={Progress} />
  <Route exact path="/editProfile" component={EditProfile} />
  <Route exact path="/editPassword" component={EditPassword} />
  <Route exact path="/allcourses" component={Allcourses} />
  <Route exact path="/searchcourse" component={SearchCourse} />
  <Route exact path="/uploadcourse" component={UploadCourse} />
  <Route exact path="/filter" component={Search} />
  <Route exact path="/admin" component={DashboardAdmin} />
  <Route component={Error404} />
  </Switch>
  <Route component={Footer} />
</Router>
  );
}

export default App;
